/** @type {import('tailwindcss').Config} */
export const content = [
  "./app/**/*.{js,ts,jsx,tsx}",
  "./pages/**/*.{js,ts,jsx,tsx}",
  "./components/**/*.{js,ts,jsx,tsx}",
];
export const theme = {
  container: {
    center: true,
    padding: '16px',
  },
  extend: {
    colors: {
      primary: '#22d3ee',
      secondary: '#64748b',
      tertiary: '#0ea5e9',
      dark: '#0f172a',
    },
    screens: {
      '2xl': '1320',
    }
  },
};
export const plugins = [];
export const darkMode = 'class';
