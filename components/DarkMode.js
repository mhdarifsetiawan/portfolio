import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setDark, setLight } from "../share/redux/darkSlice";
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import moonSvg from "./svg/Moon";
import sunSvg from "./svg/Sun";

const DarkMode = () => {
    const dispatch = useDispatch();
    const [svg , setSvg] = useState();

    // darkMode redux
    const dark = useSelector((state) => state.dark);

    useEffect(()=>{
        if (dark.isDark) {
            document.documentElement.classList.add("dark");
            setSvg(sunSvg);
        } else {
          setSvg(moonSvg)
        }
    }, [dark])
    
    const darkModeToggle = () => {
        const darkModeToggle = document.documentElement;
        if (darkModeToggle.classList.contains("dark")) {
            darkModeToggle.classList.remove("dark")
            dispatch(setLight(true))
            deleteCookie(["darkMode"])
        } else {
            darkModeToggle.classList.add("dark")
            dispatch(setDark(true))
            setCookie("darkMode", true, { maxAge: 7200 })
        }
    }

  return (
    <div className="w-9 h-9 mr-3 rounded justify-center flex items-center bg-slate-200 text-slate-300 hover:border hover:border-slate-400 hover:text-white fixed cursor-pointer bottom-4 md:bottom-7 right-4 dark:bg-slate-700" onClick={darkModeToggle}>
      {svg}
    </div>
  );
};

export default DarkMode;