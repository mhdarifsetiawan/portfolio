import Link from "next/link";
import Image from "next/image";
import noImagedefault from "../../public/portfolio/no-image-default.png"

const LatestProject = (props) => {
  console.log(props.src)
  return (
    <div className="mb-12 p-4 md:w-1/2">
      <div className="rounded-md shadow-md overflow-hidden">
        <Image src={props.src || noImagedefault} alt={props.title} width="w-full"></Image>
      </div>
      <h3 className="font-semibold text-xl text-dark mt-5 mb-2 dark:text-slate-200">
        {props.title}
      </h3>
      <div className="text-sm text-primary mb-3">
        {/* Start */}
        <span>
          <Link
            href={props.viewWeb}
            target="_blank"
            className="text-slate-400 hover:text-tertiary"
          >
            View Site
          </Link>
        </span>
        {props.repoFrontEnd && props.repoBackEnd ? (
          <>
            <span className="text-slate-400"> | </span>
            <span>
              <Link
                href={props.repoFrontEnd}
                target="_blank"
                title="Repository Frontend"
                className="text-slate-400 hover:text-tertiary"
              >
                <svg
                  width="15"
                  role="img"
                  className="fill-current inline-block mr-1"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <title>GitLab</title>
                  <path d="m23.6004 9.5927-.0337-.0862L20.3.9814a.851.851 0 0 0-.3362-.405.8748.8748 0 0 0-.9997.0539.8748.8748 0 0 0-.29.4399l-2.2055 6.748H7.5375l-2.2057-6.748a.8573.8573 0 0 0-.29-.4412.8748.8748 0 0 0-.9997-.0537.8585.8585 0 0 0-.3362.4049L.4332 9.5015l-.0325.0862a6.0657 6.0657 0 0 0 2.0119 7.0105l.0113.0087.03.0213 4.976 3.7264 2.462 1.8633 1.4995 1.1321a1.0085 1.0085 0 0 0 1.2197 0l1.4995-1.1321 2.4619-1.8633 5.006-3.7489.0125-.01a6.0682 6.0682 0 0 0 2.0094-7.003z" />
                </svg>
                Frontend
              </Link>
            </span>
            <span className="text-slate-400"> | </span>
            <span>
              <Link
                href={props.repoBackEnd}
                target="_blank"
                title="Repository Backend"
                className="text-slate-400 hover:text-tertiary"
              >
                <svg
                  width="15"
                  role="img"
                  className="fill-current inline-block mr-1"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <title>GitLab</title>
                  <path d="m23.6004 9.5927-.0337-.0862L20.3.9814a.851.851 0 0 0-.3362-.405.8748.8748 0 0 0-.9997.0539.8748.8748 0 0 0-.29.4399l-2.2055 6.748H7.5375l-2.2057-6.748a.8573.8573 0 0 0-.29-.4412.8748.8748 0 0 0-.9997-.0537.8585.8585 0 0 0-.3362.4049L.4332 9.5015l-.0325.0862a6.0657 6.0657 0 0 0 2.0119 7.0105l.0113.0087.03.0213 4.976 3.7264 2.462 1.8633 1.4995 1.1321a1.0085 1.0085 0 0 0 1.2197 0l1.4995-1.1321 2.4619-1.8633 5.006-3.7489.0125-.01a6.0682 6.0682 0 0 0 2.0094-7.003z" />
                </svg>
                Backend
              </Link>
            </span>
          </>
        ) : (
          <>
            <span className="text-slate-400"> | </span>
            <span>
              <Link
                href={props.repo}
                target="_blank"
                title="Repository Backend"
                className="text-slate-400 hover:text-tertiary"
              >
                <svg
                  width="15"
                  role="img"
                  className="fill-current inline-block mr-1"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <title>GitLab</title>
                  <path d="m23.6004 9.5927-.0337-.0862L20.3.9814a.851.851 0 0 0-.3362-.405.8748.8748 0 0 0-.9997.0539.8748.8748 0 0 0-.29.4399l-2.2055 6.748H7.5375l-2.2057-6.748a.8573.8573 0 0 0-.29-.4412.8748.8748 0 0 0-.9997-.0537.8585.8585 0 0 0-.3362.4049L.4332 9.5015l-.0325.0862a6.0657 6.0657 0 0 0 2.0119 7.0105l.0113.0087.03.0213 4.976 3.7264 2.462 1.8633 1.4995 1.1321a1.0085 1.0085 0 0 0 1.2197 0l1.4995-1.1321 2.4619-1.8633 5.006-3.7489.0125-.01a6.0682 6.0682 0 0 0 2.0094-7.003z" />
                </svg>
                Repository
              </Link>
            </span>
          </>
        )}
        {/* End */}
      </div>
      <p className="font-medium text-base text-secondary dark:text-slate-300">
        {props.description}
      </p>
    </div>
  );
};

export default LatestProject;
