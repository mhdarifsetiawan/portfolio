import Link from "next/link";
import Image from "next/image";
import myPortfolio1 from "../../public/portfolio/Landing-Page Website-Game.jpg";
import myPortfolio2 from "../../public/portfolio/Dashboard-Website-Game.jpg";

const LatestProject = (props) => {
  return (
    <div className="w-full px-4 flex flex-wrap justify-center xl:w-10/12 xl:mx-auto">
      <div className="mb-12 p-4 md:w-1/2">
        <div className="rounded-md shadow-md overflow-hidden">
          <Image
            src={myPortfolio1}
            alt="landingpage game portfolio"
            width="w-full"
          ></Image>
        </div>
        <h3 className="font-semibold text-xl text-dark mt-5 mb-2">
          Game Website (Frontend & Backend)
        </h3>
        <div className="text-sm text-primary mb-3">
          <span>
            <Link
              href="https://fe-next-fsw23.vercel.app/"
              target="_blank"
              className="text-slate-400 hover:text-primary"
            >
              View Site
            </Link>
          </span>
          <span> | </span>
          <span>
            <Link
              href="https://gitlab.com/zilianrifaldof/fe-titik_koma-next-fsw_23"
              target="_blank"
              title="Repository Frontend"
              className="text-slate-400 hover:text-primary"
            >
              <svg
                width="15"
                role="img"
                className="fill-current inline-block mr-1"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>GitLab</title>
                <path d="m23.6004 9.5927-.0337-.0862L20.3.9814a.851.851 0 0 0-.3362-.405.8748.8748 0 0 0-.9997.0539.8748.8748 0 0 0-.29.4399l-2.2055 6.748H7.5375l-2.2057-6.748a.8573.8573 0 0 0-.29-.4412.8748.8748 0 0 0-.9997-.0537.8585.8585 0 0 0-.3362.4049L.4332 9.5015l-.0325.0862a6.0657 6.0657 0 0 0 2.0119 7.0105l.0113.0087.03.0213 4.976 3.7264 2.462 1.8633 1.4995 1.1321a1.0085 1.0085 0 0 0 1.2197 0l1.4995-1.1321 2.4619-1.8633 5.006-3.7489.0125-.01a6.0682 6.0682 0 0 0 2.0094-7.003z" />
              </svg>
              Frontend
            </Link>
          </span>
          <span> | </span>
          <span>
            <Link
              href="https://gitlab.com/mhdarifsetiawan/be-aku-cinta-binar-fsw-23"
              target="_blank"
              title="Repository Backend"
              className="text-slate-400 hover:text-primary"
            >
              <svg
                width="15"
                role="img"
                className="fill-current inline-block mr-1"
                viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>GitLab</title>
                <path d="m23.6004 9.5927-.0337-.0862L20.3.9814a.851.851 0 0 0-.3362-.405.8748.8748 0 0 0-.9997.0539.8748.8748 0 0 0-.29.4399l-2.2055 6.748H7.5375l-2.2057-6.748a.8573.8573 0 0 0-.29-.4412.8748.8748 0 0 0-.9997-.0537.8585.8585 0 0 0-.3362.4049L.4332 9.5015l-.0325.0862a6.0657 6.0657 0 0 0 2.0119 7.0105l.0113.0087.03.0213 4.976 3.7264 2.462 1.8633 1.4995 1.1321a1.0085 1.0085 0 0 0 1.2197 0l1.4995-1.1321 2.4619-1.8633 5.006-3.7489.0125-.01a6.0682 6.0682 0 0 0 2.0094-7.003z" />
              </svg>
              Backend
            </Link>
          </span>
        </div>
        <p className="font-medium text-base text-secondary">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
          mollis tempor quam eget vestibulum. Duis ornare purus neque, at
          imperdiet est rhoncus at. Suspendisse fermentum.
        </p>
      </div>
      <div className="mb-12 p-4 md:w-1/2">
        <div className="rounded-md shadow-md overflow-hidden">
          <Image
            src={myPortfolio2}
            alt="e-commerce portfolio"
            width="w-full"
          ></Image>
        </div>
        <h3 className="font-semibold text-xl text-dark mt-5 mb-3">
          E-commerce
        </h3>
        <p className="font-medium text-base text-secondary">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
          mollis tempor quam eget vestibulum. Duis ornare purus neque, at
          imperdiet est rhoncus at. Suspendisse fermentum.
        </p>
      </div>
      <div className="mb-12 p-4 md:w-1/2">
        <div className="rounded-md shadow-md overflow-hidden">
          <Image
            src={myPortfolio3}
            alt="ui design portfolio"
            width="w-full"
          ></Image>
        </div>
        <h3 className="font-semibold text-xl text-dark mt-5 mb-3">UI Design</h3>
        <p className="font-medium text-base text-secondary">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
          mollis tempor quam eget vestibulum. Duis ornare purus neque, at
          imperdiet est rhoncus at. Suspendisse fermentum.
        </p>
      </div>
      <div className="mb-12 p-4 md:w-1/2">
        <div className="rounded-md shadow-md overflow-hidden">
          <Image src={myPortfolio4} alt="create API" width="w-full"></Image>
        </div>
        <h3 className="font-semibold text-xl text-dark mt-5 mb-3">
          Create API
        </h3>
        <p className="font-medium text-base text-secondary">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
          mollis tempor quam eget vestibulum. Duis ornare purus neque, at
          imperdiet est rhoncus at. Suspendisse fermentum.
        </p>
      </div>
    </div>
  );
};

export default LatestProject;
