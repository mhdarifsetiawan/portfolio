import Link from "next/link"
import Image from "next/image"
import noImagedefault from "../../public/portfolio/no-image-default.png"

const LatestJobs = (props) => {
    let jobDesc, tech
    if (props.jobDesc && typeof props.jobDesc === 'object') {
        jobDesc = props.jobDesc.map((djo) => <li>{djo}</li>)
    } else {
        jobDesc = props.jobDesc
    }

    if (props.tech) {
        tech = props.tech.map((t) => {
        return (
        <li>
            <div className="flex items-center rounded-full bg-teal-200 px-3 py-1 me-1 mb-1 text-xs font-semiboldbold  text-secondary">{t}
            </div>
        </li>)
    })
    }

    return (
        <div className="mb-5 p-4 md:w-full">
            <div className="md:w-full flex flex-wrap flex-col md:flex-row">
                <div className="w-full md:w-1/4">
                    <span className="text-secondary text-sm font-semibold">2023 - Present</span>
                </div>
                <div className="w-full md:w-3/4">
                    <h3 className="font-semibold text-lg text-dark mt-1 md:mt-0 mb-0 dark:text-slate-200">{props.role} - {props.company}</h3>
                    {/* <h3 className="font-semibold text-base text-dark mt-2 mb-2 dark:text-slate-200">
                        {props.company}
                    </h3> */}
                    <div className="text-sm text-primary mb-3">
                {/* Start */}
                <span>
                    <Link
                        href={props.viewWeb + '?source=masofficial.my.id'}
                        target="_blank"
                        className="text-slate-400 hover:text-tertiary"
                    >
                        Visit Company
                    </Link>
                </span>
                {props.repoFrontEnd && props.repoBackEnd ? (
                    <>
                        <span className="text-slate-400"> | </span>
                        <span>
                            <Link
                                href={props.repoFrontEnd}
                                target="_blank"
                                title="Repository Frontend"
                                className="text-slate-400 hover:text-tertiary"
                            >
                                <svg
                                    width="15"
                                    fill="#000000"
                                    role="img"
                                    className="fill-current inline-block mr-1"
                                    viewBox="0 0 24 24"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <title>GitLab</title>
                                    <path d="M21 12C21 16.9706 16.9706 21 12 21M21 12C21 7.02944 16.9706 3 12 3M21 12C21 13.6569 16.9706 15 12 15C7.02944 15 3 13.6569 3 12M21 12C21 10.3431 16.9706 9 12 9C7.02944 9 3 10.3431 3 12M12 21C7.02944 21 3 16.9706 3 12M12 21C10.3431 21 9 16.9706 9 12C9 7.02944 10.3431 3 12 3M12 21C13.6569 21 15 16.9706 15 12C15 7.02944 13.6569 3 12 3M3 12C3 7.02944 7.02944 3 12 3" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                Frontend
                            </Link>
                        </span>
                        <span className="text-slate-400"> | </span>
                        <span>
                            <Link
                                href={props.repoBackEnd}
                                target="_blank"
                                title="Repository Backend"
                                className="text-slate-400 hover:text-tertiary"
                            >
                                <svg
                                    width="15"
                                    role="img"
                                    className="fill-current inline-block mr-1"
                                    viewBox="0 0 24 24"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <title>GitLab</title>
                                    <path d="m23.6004 9.5927-.0337-.0862L20.3.9814a.851.851 0 0 0-.3362-.405.8748.8748 0 0 0-.9997.0539.8748.8748 0 0 0-.29.4399l-2.2055 6.748H7.5375l-2.2057-6.748a.8573.8573 0 0 0-.29-.4412.8748.8748 0 0 0-.9997-.0537.8585.8585 0 0 0-.3362.4049L.4332 9.5015l-.0325.0862a6.0657 6.0657 0 0 0 2.0119 7.0105l.0113.0087.03.0213 4.976 3.7264 2.462 1.8633 1.4995 1.1321a1.0085 1.0085 0 0 0 1.2197 0l1.4995-1.1321 2.4619-1.8633 5.006-3.7489.0125-.01a6.0682 6.0682 0 0 0 2.0094-7.003z" />
                                </svg>
                                Backend
                            </Link>
                        </span>
                    </>
                ) : (
                    <>
                        {/* <span className="text-slate-400"> | </span>
                        <span>
                            <Link
                                href={props.repo}
                                target="_blank"
                                title="Repository Backend"
                                className="text-slate-400 hover:text-tertiary"
                            >
                                <svg
                                    width="15"
                                    role="img"
                                    className="fill-current inline-block mr-1"
                                    viewBox="0 0 24 24"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <title>GitLab</title>
                                    <path d="m23.6004 9.5927-.0337-.0862L20.3.9814a.851.851 0 0 0-.3362-.405.8748.8748 0 0 0-.9997.0539.8748.8748 0 0 0-.29.4399l-2.2055 6.748H7.5375l-2.2057-6.748a.8573.8573 0 0 0-.29-.4412.8748.8748 0 0 0-.9997-.0537.8585.8585 0 0 0-.3362.4049L.4332 9.5015l-.0325.0862a6.0657 6.0657 0 0 0 2.0119 7.0105l.0113.0087.03.0213 4.976 3.7264 2.462 1.8633 1.4995 1.1321a1.0085 1.0085 0 0 0 1.2197 0l1.4995-1.1321 2.4619-1.8633 5.006-3.7489.0125-.01a6.0682 6.0682 0 0 0 2.0094-7.003z" />
                                </svg>
                                Repository
                            </Link>
                        </span> */}
                    </>
                )}
                {/* End */}
                    </div>
                    <div className="font-medium text-base text-secondary dark:text-slate-300">
                        <span>Job Desc :</span>
                        <ul className={"list-disc list-inside"}>
                            {/* {typeof props.description === 'object' ? props.description.map((djo) => <JobOverview dataJobsOverview={djo} />) : <li>'HAHA'</li>} */}
                            {jobDesc}
                        </ul>
                    </div>
                    <div className="w-full mt-5">
                        <ul className="flex flex-wrap">
                            {tech}
                        </ul>
                    </div>
                </div>
            </div>
            {/* <div className="rounded-md shadow-md overflow-hidden">
                <Image src={props.src || noImagedefault} alt={props.title} width="w-full"></Image>
            </div> */}
        </div>
    );
};

export default LatestJobs;
