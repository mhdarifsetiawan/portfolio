const Button = (props) => {
    const { text = "Submit", className = "text-base font-semibold border border-primary px-8 py-3 hover:bg-primary hover:shadow-lg hover:opacity-80 transition duration-300 ease-in-out", background = "", color = "text-primary", colorHover = "hover:text-white", width = "" } = props;
  return <button className={className + " " + width + " " + color + " " + colorHover + " " + background}>{text}</button>;
};

export default Button;
