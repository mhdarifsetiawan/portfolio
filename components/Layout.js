import DarkMode from "./DarkMode";
import { useDispatch, useSelector } from "react-redux";
import { getCookies, getCookie, setCookie, deleteCookie } from 'cookies-next';
import { useEffect, useState } from "react";
import { setDark, setLight } from "../share/redux/darkSlice";

export default function Layout({ children }) {
    const cookies = getCookies();
    const dispatch = useDispatch();
    const dark = useSelector((state) => state.dark);

    useEffect(() => {
      if (cookies.darkMode !== "undefined" && cookies.darkMode) {
        dispatch(setDark(cookies.darkMode));
      }
    }, [dark]);

    return (
      <>
        <main>{children}</main>
        <DarkMode/>
      </>
    )
  }