import { useState } from "react";
import Button from "./Button";
import LoadingSpinner from "./LoadingSpinner";
import { ErrorMessage, SuccessMessage } from './Message'

const Contact = () => {
  const [fullname, setFullname] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [form, setForm] = useState("");

  const onSubmitForm = async (e) => {
    e.preventDefault();

    if (fullname && email && message) {
      setForm({ state: "loading" });
      try {
        const res = await fetch(`api/contact`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ fullname, email, message }),
        });

        const { error } = await res.json();

        if (error) {
          setForm({
            state: "error",
            message: error,
          });
          return;
        }

        setForm({
            state: 'success',
            message: 'Your message was sent successfully.',
        })
        setFullname("");
        setEmail("");
        setMessage("");
      } catch (error) {
        setForm({
        	state: 'error',
        	message: 'Something went wrong',
        })
      }
    }
  };

  return (
    <form onSubmit={(e) => onSubmitForm(e)}>
      <div className="w-full lg:w-2/3 lg:mx-auto">
        <div className="w-full px-4 mb-8">
          <label for="fullname" className="text-base font-bold text-primary">
            Full Name
          </label>
          <input
            type="text"
            id="fullname"
            name="fullname"
            placeholder="Your name"
            className="w-full bg-slate-200 text-dark p-3 rounded-md focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary dark:bg-slate-300 dark:placeholder-slate-500"
            value={fullname}
            required
            onChange={(e) => {
              setFullname(e.target.value);
            }}
          />
        </div>
        <div className="w-full px-4 mb-8">
          <label for="email" className="text-base font-bold text-primary">
            Email
          </label>
          <input
            type="email"
            id="email"
            name="email"
            placeholder="youremail@gmail.com"
            className="w-full bg-slate-200 text-dark p-3 rounded-md focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary dark:bg-slate-300 dark:placeholder-slate-500"
            value={email}
            required
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
        </div>
        <div className="w-full px-4 mb-8">
          <label for="message" className="text-base font-bold text-primary">
            Your Message
          </label>
          <textarea
            type="text"
            id="message"
            name="message"
            placeholder="Your message"
            className="w-full bg-slate-200 text-dark p-3 rounded-md focus:outline-none focus:ring-primary focus:ring-1 focus:border-primary h-36 dark:bg-slate-300 dark:placeholder-slate-500"
            value={message}
            required
            onChange={(e) => {
              setMessage(e.target.value);
            }}
          />
        </div>
        <div className="mb-2">
            <span>
                {form.state === "loading" && <LoadingSpinner />}
                {form.state === "error" ? (
                <ErrorMessage>{form.message}</ErrorMessage>
                ) : form.state === "success" ? (
                <SuccessMessage>{form.message}</SuccessMessage>
                ) : (
                ""
                )}
            </span>
        </div>
        <div className="w-full lg:w-1/3 mx-auto px-4">
          {/* <button className="text-base font-semibold text-white bg-primary py-3 px-8 rounded-full w-full hover:opacity-80 hover:shadow-lg transition duration-500">
            Send
          </button> */}
          <Button text={"Send"} width={"w-full"} colorHover={"hover:text-white dark:hover:text-slate-900"}></Button>
        </div>
      </div>
    </form>
  );
};

export default Contact;
