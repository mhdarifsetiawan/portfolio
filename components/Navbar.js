import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import logoLight from "../public/logo/logo.svg";
import logoDark from "../public/logo/logowhite.png";
import { useSelector } from "react-redux";

const Navbar = () => {
  // darkMode redux
  const dark = useSelector((state) => state.dark);

  const [logo, setLogo] = useState(logoLight)

  useEffect(() => {
    if (dark.isDark) {
      setLogo(logoDark)
    } 
    else {
      setLogo(logoLight)
    }
  }, [dark])

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  });

  const handleScroll = () => {
    const header = document.querySelector("header");
    const fixedNav = header.offsetTop+150;

    if (window.pageYOffset > fixedNav) {
      header.classList.add("navbarFix");
    } else {
      header.classList.remove("navbarFixDark");
      header.classList.remove("navbarFix");
    }

  };

  function handleClickHamburger() {
    const hamburger = document.getElementById("hamburger");
    const navMenu = document.getElementById("navMenu");
    hamburger.classList.toggle("hamburgerActive");
    navMenu.classList.toggle("hidden");
    navMenu.classList.toggle("dark:bg-dark");
  }

  return (
    <div className="flex items-center justify-between relative">
      <div className="px-4">
        <Link
          href="/"
          className="font-bold text-lg text-primary flex items-center py-6"
        >
          <div>
            {/* {dark == true ? (<Image src={logoDark} width="100" alt="logo"></Image>) : (<Image src={logoLight} width="100" alt="logo"></Image>) } */}
            <Image src={logo} width="100" alt="logo"></Image>
          </div>
          {/* <div id="logo" className="text-3xl">{logo}</div> */}
        </Link>
      </div>
      <div className="flex items-center px-4">
        <button
          id="hamburger"
          name="hamburger"
          className="block absolute right-4 lg:hidden"
          onClick={handleClickHamburger}
        >
          <span className="hamburgerLine transition duration-300 ease-in-out origin-top-left"></span>
          <span className="hamburgerLine"></span>
          <span className="hamburgerLine transition duration-300 ease-in-out origin-bottom-left"></span>
        </button>
        <nav
          id="navMenu"
          className="hidden absolute py-5 bg-white shadow-lg rounded-lg max-w-[250px] w-full right-4 top-full lg:block lg:static lg:bg-transparent lg:max-w-full lg:shadow-none lg:rounded-none"
        >
          <ul className="block lg:flex">
            <li className="group">
              <Link
                href="#home"
                className="text-base text-dark dark:text-white py-2 mx-8 flex group-hover:text-primary"
                scroll={false}
              >
                Home
              </Link>
            </li>
            <li className="group">
              <Link
                href="#about"
                className="text-base text-dark dark:text-white py-2 mx-8 flex group-hover:text-primary"
                scroll={false}
              >
                About
              </Link>
            </li>
            <li className="group">
              <Link
                href="#latestJob"
                className="text-base text-dark dark:text-white py-2 mx-8 flex group-hover:text-primary"
                scroll={false}
              >
                Work Experience
              </Link>
            </li>
            <li className="group">
              <Link
                href="#portfolio"
                className="text-base text-dark dark:text-white py-2 mx-8 flex group-hover:text-primary"
                scroll={false}
              >
                Portfolio
              </Link>
            </li>
            <li className="group">
              <Link
                href="#contact"
                className="text-base text-dark dark:text-white py-2 mx-8 flex group-hover:text-primary"
                scroll={false}
              >
                Contact
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default Navbar;
