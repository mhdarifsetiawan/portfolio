import darkSlice from "./darkSlice";

const { configureStore } = require("@reduxjs/toolkit");

export const store = configureStore({
  reducer: {
    dark: darkSlice,
  },
});
