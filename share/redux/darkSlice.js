import { createSlice } from "@reduxjs/toolkit";

export const darkSlice = createSlice({
//   name: "auth",
  name: "dark",
  initialState: {
    isDark: false,
  },
  reducers: {
    // reducer to update state to loggin is true and add jwt token
    setDark: (state, action) => {
      state.isDark = true;
    },
    // reducer to update state to loggin is false and remove jwt
    setLight: (state, action) => {
      state.isDark = false;
    },
  },
});

export const { setDark, setLight } = darkSlice.actions;
export default darkSlice.reducer;
