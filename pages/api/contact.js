import nodemailer from "nodemailer";

export default async (req, res) => {
  const { fullname, email, message } = req.body;
  const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: process.env.MY_APP_EMAIL,
      pass: process.env.MY_APP_PASS,
    },
  });

  try {
    await transporter.sendMail({
      from: email,
      to: process.env.MY_APP_EMAIL_RECEIVER_ONE,
      replyTo: email,
      subject: `Contact form submission from ${fullname}`,
      html: `<p>You have a contact form submission</p><br>
        <p><strong>Email: </strong> ${email}</p><br>
        <p><strong>Message: </strong></p><br>
        <p>${message}</p>
      `,
    });
  } catch (error) {
    return res.status(500).json({ error: error.message || error.toString() });
  }
  return res.status(200).json({ error: "" });
};
