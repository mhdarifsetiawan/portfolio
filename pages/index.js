import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useEffect } from "react";
// import styles from "../styles/Home.module.css";
import myProfilePhoto from "../public/my-profile-photo.png";
import myPortfolio1 from "/public/portfolio/Landing-Page Website-Game.jpg";
import myPortfolio2 from "/public/portfolio/Dashboard-Website-Game.jpg";
import myPortfolio3 from "/public/portfolio/bootstrap-portfolio.png";
import logo from "../public/logo/logowhite.png";
import LatestProject from "../components/HomePage/LatestProject";
import Navbar from "../components/Navbar";
import MySocialMedia from "../components/MySocialMedia";
import Contact from "../components/Contact";
import Button from "../components/Button";
import MyContact from "../components/MyContact";
import LatestJobs from "../components/HomePage/LatestJobs";

export default function Home() {
  return (
    <div>
      <Head>
        <title>My Portfolio | Mhd Arif Setiawan</title>
        <meta name="description" content="masofficial portfolio, fullstack web and mobile developer" />
        <meta name="google-site-verification" content="mCceEq4jj2SXi8LHRUQc99t5T8ey4HJELHsrODJwIoE" />
        <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon.png"/>
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png"/>
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png"/>
        <link rel="manifest" href="/favicon/site.webmanifest"/>
        <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5"/>
        <meta name="msapplication-TileColor" content="#da532c"/>
        <meta name="theme-color" content="#ffffff"></meta>
      </Head>
      {/* Header section start */}
      <header className="bg-transparent absolute top-0 left-0 w-full flex items-center z-10 text-dark dark:text-white">
        <div className="container">
          <Navbar></Navbar> 
        </div>
      </header>
      {/* Header section end */}
      {/* Hero section start */}
      <section id="home" className="pt-36 dark:bg-gray-900">
        <div className="container">
          <div className="flex flex-wrap">
            <div className="w-full self-center px-4 lg:w-1/2">
              <h1 className="text-base font-semibold text-primary md:text-xl">
                Hai 👋, I am{" "}
                <span className="block font-bold text-4xl mt-1">
                  Muhammad Arif Setiawan
                </span>
              </h1>
              <h2 className="font-medium text-secondary text-lg mb-5 lg:text-2xl dark:text-slate-300">
                Full-stack Web & Mobile Developer
              </h2>
              <p className="font-medium text-secondary mb-10 leading-relaxed dark:text-slate-400">
                I have a great interest in technology, especially web
                development. I am able to make a website for your business or
                your product, so that it looks more attractive, up-to-date, and
                has high value.
              </p>
              <Link href="#contact" scroll={false}>
                <Button text={"Contact Me"} colorHover={"hover:text-white dark:hover:text-slate-900"}/>
              </Link>
            </div>
            <div className="w-full self-end px-4 lg:w-1/2">
              <div className="relative mt-24 sm:mt-0 lg:right-0">
                <Image
                  src={myProfilePhoto}
                  alt="profile picture author"
                  className="max-w-full mx-auto dark:opacity-70"
                ></Image>
                <span className="absolute bottom-0 -z-10 left-1/2 -translate-x-1/2 md:scale-125">
                  <svg
                    width="400"
                    height="400"
                    viewBox="0 0 200 200"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fill="#22d3ee"
                      d="M39.5,-50.1C52.1,-45.2,63.8,-34.9,65.7,-22.8C67.6,-10.7,59.8,3.1,52.4,14.3C45.1,25.6,38.1,34.2,29.5,40.3C20.8,46.3,10.4,49.8,-1.8,52.2C-13.9,54.6,-27.8,56,-37.6,50.4C-47.4,44.7,-53.2,31.9,-56,19.1C-58.9,6.4,-59,-6.4,-58.4,-22.3C-57.9,-38.3,-56.7,-57.4,-46.9,-63.2C-37,-69,-18.5,-61.5,-2.5,-58C13.5,-54.6,26.9,-55.1,39.5,-50.1Z"
                      transform="translate(100 100) scale(1.1)"
                    />
                  </svg>
                </span>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* Hero section end */}
      {/* About section start */}
      <section id="about" className="pt-36 pb-32 dark:bg-gray-900">
        <div className="container">
          <div className="flex flex-wrap">
            <div className="w-full px-4 mb-10 lg:w-1/2">
              <h4 className="font-bold uppercase text-primary text-lg mb-3">
                About Me
              </h4>
              <h2 className="font-bold text-dark text-3xl mb-5 max-w-md lg:text-4xl dark:text-slate-200">
                Programming is Unlimited Creativity
              </h2>
              <p className="font-medium text-base text-secondary max-w-xl lg:text-lg dark:text-slate-300">
                I'm not an IT graduate, I'm just a self-taught programmer. But I
                have a passion in website development beyond what you think. I
                am used to working independently or as a team on a project.
              </p>
            </div>
            <div className="w-full px-4 lg:w-1/2">
              <h3 className="font-semibold text-dark text-2xl mb-4 lg:text-3xl lg:pt-10 dark:text-slate-200">
                Let's be Friends
              </h3>
              <p className="font-medium text-base text-secondary mb-6 lg:text-lg dark:text-slate-300">
                If you are looking for a cheerful and funny person, then we are
                the perfect match. Sometimes big ideas come up in casual & funny
                conversation
              </p>
              <div className="flex items-center">
                <MySocialMedia></MySocialMedia>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* About section end */}
      {/* Latest Jobs section start */}
      <section id="latestJob" className="pt-36 pb-16 bg-slate-100 dark:bg-gray-800">
        <div className="container">
          <div className="w-full px-4">
            <div className="max-w-xl mx-auto text-center mb-16">
              <h4 className="font-semibold text-lg text-primary mb-2">
                Career
              </h4>
              <h2 className="font-bold text-dark text-3xl mb-4 sm:text-4xl lg:text-5xl dark:text-slate-200">
                Work Experience
              </h2>
              <p className="font-medium text-base text-secondary md:text-lg dark:text-slate-300">
              The following is my work experience.
              </p>
            </div>
          </div>
          <div className="w-full px-4 flex flex-wrap justify-center xl:w-1/2 xl:mx-auto">
            <LatestJobs
                // src={myPortfolio1}
                role="Fullstack Developer"
                company="PT Bimasakti Multi Sinergi"
                jobDesc={['Maintenance of existing projects, such as API, mobile and web frontend', 'Initiating the creation of new applications according to client (company) requests for sales support purposes.']}
                viewWeb="https://www.bm.co.id"
                // repoFrontEnd="https://gitlab.com/zilianrifaldof/fe-titik_koma-next-fsw_23"
                // repoBackEnd="https://gitlab.com/mhdarifsetiawan/be-aku-cinta-binar-fsw-23"
                tech={['JavaScript', 'TypeScript', 'ReactJS', 'React Native', 'Laravel', 'NodeJS', 'PHP']}
            ></LatestJobs>
          </div>
           {/* START HIRE ME */}
          <hr className="w-full xl:w-1/2 mx-auto" />
          <div className="w-full px-4 flex flex-wrap justify-center xl:w-1/2 xl:mx-auto text-center">
              <span className="w-full p-4 font-bold text-2xl text-dark dark:text-slate-200">want to collaborate or hire me?</span>
              <span className="w-full px-4 text-secondary dark:text-slate-300">Please, Contact me on:</span>
            </div>
            <div className="flex items-center flex-wrap justify-center text-center py-2">
              <MyContact/>
            </div>
            {/* END HIRE ME */}
        </div>
      </section>
      {/* Latest Jobs section end */}
      {/* Portfolio section start */}
      <section id="portfolio" className="pt-36 pb-16 dark:bg-gray-900">
        <div className="container">
          <div className="w-full px-4">
            <div className="max-w-xl mx-auto text-center mb-16">
              <h4 className="font-semibold text-lg text-primary mb-2">
                Personal Portfolio
              </h4>
              <h2 className="font-bold text-dark text-3xl mb-4 sm:text-4xl lg:text-5xl dark:text-slate-200">
                Latest Project
              </h2>
              <p className="font-medium text-base text-secondary md:text-lg dark:text-slate-300">
              You can see the last few projects I've worked on. Some projects may still be updated/changed. Some of the projects I work on involve several contributors.
              </p>
            </div>
          </div>
          <div className="w-full px-4 flex flex-wrap justify-center xl:w-10/12 xl:mx-auto">
            <LatestProject
              src={myPortfolio1}
              title="Website Game Traditional"
              description="Frontend with NextJS, Backend with ExpressJS, Bootstrap CSS, Redux for state management, PostgreSQL database, Swagger for API Doc"
              viewWeb="https://fe-next-fsw23.vercel.app/"
              repoFrontEnd="https://gitlab.com/zilianrifaldof/fe-titik_koma-next-fsw_23"
              repoBackEnd="https://gitlab.com/mhdarifsetiawan/be-aku-cinta-binar-fsw-23"
            ></LatestProject>
            {/* <LatestProject
              src={myPortfolio2}
              title="Dashboard User"
              description="This dashboard is created using NextJS as the frontend and ExpressJS as the backend. We also created an API for use on the frontend, while authentication uses JWT tokens. And use redux as state management. On the dashboard, users can create game rooms, edit profiles, play games vs. computers, or vs other players."
              viewWeb="https://fe-next-fsw23.vercel.app/login"
              repoFrontEnd="https://gitlab.com/zilianrifaldof/fe-titik_koma-next-fsw_23"
              repoBackEnd="https://gitlab.com/mhdarifsetiawan/be-aku-cinta-binar-fsw-23"
            ></LatestProject> */}
            <LatestProject 
              src={myPortfolio3}
              title="Bootstrap UI"
              description="Bootstrap, SCSS, JavaScript"
              viewWeb="https://my-portfolio-bootstrap-v1.vercel.app/"
              repo="https://gitlab.com/mhdarifsetiawan/bootstrap-templates-v1"
              ></LatestProject>
          </div>
        </div>
      </section>
      {/* Portfolio section end */}
      {/* Contact section start */}
      <section id="contact" className="pt-36 pb-32 bg-slate-100 dark:bg-gray-800">
        <div className="container">
          <div className="w-full px-4">
            <div className="max-w-xl mx-auto text-center mb-16">
              <h4 className="font-semibold text-lg text-primary mb-2">
                Contact
              </h4>
              <h2 className="font-bold text-dark text-3xl mb-4 sm:text-4xl lg:text-5xl dark:text-slate-200">
                Contact us
              </h2>
              <p className="font-medium text-base text-secondary md:text-lg dark:text-slate-300">
                If you want to collaborate or have any questions, opportunities,
                or might simply want to say hello then, feel free to fill out my
                contact form and I'll without a doubt hit you up in a hurry.
              </p>
              <p className="font-medium text-base text-secondary mt-1 md:text-lg dark:text-slate-300">Or if you would prefer to, you can also reach me on:</p>
                <div className="flex items-center justify-center my-2">
                  <MyContact></MyContact>
                </div>
            </div>
          </div>
          <Contact></Contact>
        </div>
      </section>
      {/* Contact section end */}
      {/* Footer section start */}
      <footer className="bg-dark pt-24 pb-12">
        <div className="container">
          <div className="flex flex-wrap">
            <div className="w-full px-4 mb-12 text-slate-300 font-medium md:w-1/3">
              <h2 className="font-bold text-4xl text-white mb-5">
                <Link href="/">
                  <Image src={logo} width="200" alt="logo"></Image>
                </Link>
              </h2>
              <h3 className="font-bold text-2xl mb-2">Contact me</h3>
              <p>
                <Link href="mailto:mhdarifsetiawan01@gmail.com">
                  mhdarifsetiawan01@gmail.com
                </Link>
              </p>
              <p>Bandung, West Java, Indonesia</p>
            </div>
            <div className="w-full px-4 mb-12 md:w-1/3">
              <h3 className="font-semibold text-xl text-white mb-5">
                Category
              </h3>
              <ul className="text-slate-300">
                <li>
                  <Link
                    href="#"
                    className="inline-block text-base hover:text-primary mb-3"
                  >
                    Programming
                  </Link>
                </li>
                <li>
                  <Link
                    href="#"
                    className="inline-block text-base hover:text-primary mb-3"
                  >
                    Technology
                  </Link>
                </li>
                <li>
                  <Link
                    href="#"
                    className="inline-block text-base hover:text-primary mb-3"
                  >
                    Life style
                  </Link>
                </li>
              </ul>
            </div>
            <div className="w-full px-4 mb-12 md:w-1/3">
              <h3 className="font-semibold text-xl text-white mb-5">Link</h3>
              <ul className="text-slate-300">
                <li>
                  <Link
                    href="#home"
                    className="inline-block text-base hover:text-primary mb-3"
                    scroll={false}
                  >
                    Home
                  </Link>
                </li>
                <li>
                  <Link
                    href="#about"
                    className="inline-block text-base hover:text-primary mb-3"
                    scroll={false}
                  >
                    About me
                  </Link>
                </li>
                <li>
                  <Link
                    href="#portfolio"
                    className="inline-block text-base hover:text-primary mb-3"
                    scroll={false}
                  >
                    Portfolio
                  </Link>
                </li>
                <li>
                  <Link
                    href="#contact"
                    className="inline-block text-base hover:text-primary mb-3"
                    scroll={false}
                  >
                    Contact
                  </Link>
                </li>
              </ul>
            </div>
          </div>
          <div className="w-full pt-10 border-t border-slate-700">
            <div className="flex items-center justify-center mb-5">
              <MySocialMedia></MySocialMedia>
            </div>
            <p className="font-medium text-xs text-slate-500 text-center">
              Made with <span className="text-pink-500">❤</span> by{" "}
              <Link
                href="https://www.instagram.com/mhdarifsetiawan/"
                target="_blank"
                className="font-bold text-primary"
              >
                Muhammad Arif Setiawan
              </Link>{" "}
              using{" "}
              <Link
                href="https://nextjs.org/"
                target="_blank"
                className="font-bold text-sky-500"
              >
                NextJS
              </Link>{" "}
              and{" "}
              <Link
                href="https://tailwindcss.com/"
                target="_blank"
                className="font-bold text-sky-500"
              >
                Taildwind CSS
              </Link>
            </p>
          </div>
        </div>
      </footer>
      {/* Footer section end */}
    </div>
  );
}
