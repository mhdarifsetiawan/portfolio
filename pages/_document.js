import { Html, Head, Main, NextScript } from 'next/document';

const Document = (props) => {
  
  return (
    <Html className='scroll-smooth' style={{scrollBehavior:'smooth'}}>
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}

export default Document;